﻿using PathCreation;
using UnityEngine;

namespace PathCreation.Examples {

    [ExecuteInEditMode]
    public class PathPlacer : PathSceneTool {

        //public GameObject[] prefabs; //.
        public GameObject prefab;
        public GameObject holder;
        public float spacing = 3;
        //public float minOffset, maxOffset;

        const float minSpacing = .1f;

        float offset = 0;

        public void Generate () { //.
            if (pathCreator != null && prefab != null && holder != null) {
                DestroyObjects ();

                //BezierPath bezier = pathCreator.bezierPath;
                VertexPath path = pathCreator.path;

                spacing = Mathf.Max(minSpacing, spacing);
                float dst = 6;

                while (dst < path.length) {
                    //offset = Random.Range(minOffset, maxOffset);
                    Vector3 point = path.GetPointAtDistance(dst);// + new Vector3(offset, 0, 0);//bezier.GetPoint(path.NumPoints - 1);
                    Quaternion rot = path.GetRotationAtDistance(dst);
                    Instantiate (prefab, point, rot *= Quaternion.Euler(-90, 0, 0), holder.transform); //.
                    dst += spacing;
                }
            }
        }

        void DestroyObjects () {
            int numChildren = holder.transform.childCount;
            for (int i = numChildren - 1; i >= 0; i--) {
                DestroyImmediate (holder.transform.GetChild (i).gameObject, false);
            }
        }

        protected override void PathUpdated () {
            if (pathCreator != null) {
                Generate ();
            }
        }
    }
}