﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class FollowPath : MonoBehaviour
{
    Vector3 pointA;
    Vector3 pointB;
    GameObject player;
    PathGenerator pathGenerator;

    public PathCreator pathCreator;

    float distanceTravelled;
    bool isGenCooldown;

    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        pathGenerator = pathCreator.gameObject.GetComponent<PathGenerator>();
        isGenCooldown = false;
    }

    // Update is called once per frame
    void Update()
    {
        distanceTravelled += speed * Time.deltaTime;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled) + new Vector3(0,0.25f,0);
        transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);

        if (Vector3.Distance(player.transform.position, pathCreator.bezierPath.GetPointsInSegment(2)[0]) < pathGenerator.genDistance && !isGenCooldown)
        {
            pointA = pathCreator.bezierPath.GetPointsInSegment(0)[0];
            pointB = pathCreator.bezierPath.GetPointsInSegment(1)[0];
            distanceTravelled -= Vector3.Distance(pointA, pointB);
            isGenCooldown = true;
            Invoke("GenerationCooldown", pathGenerator.genCooldownValue);
        }
    }
    void GenerationCooldown()
    {
        isGenCooldown = false;
    }
}
