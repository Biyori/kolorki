﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    Renderer rend;

    public Material[] materials;

    bool isDissolving;
    int currentMaterial;
    float dissolveAmount;

    // Start is called before the first frame update
    void Awake()
    {
        isDissolving = false;
        rend = GetComponent<Renderer>();
        currentMaterial = Random.Range(0, materials.Length);
        rend.material = materials[currentMaterial];
    }

    // Update is called once per frame
    void Update()
    {
        if (isDissolving)
        {
            dissolveAmount = Mathf.Clamp01(dissolveAmount + Time.deltaTime);
            rend.material.SetFloat("_DissolveAmount", dissolveAmount);
        }

        if (dissolveAmount == 1)
            Destroy(this.gameObject);

    }

    public void Dissolve()
    {
        isDissolving = true;
    }
}
