﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverScreen;
    public GameObject levelCompleteScreen;

    bool isGameOver;


    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
        //gameOverScreen = GameObject.Find("GameOver");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GameOver()
    {
        if (!isGameOver)
        {
            isGameOver = true;
            gameOverScreen.SetActive(true);
        }
    }

    public bool IsGameOver()
    {
        return isGameOver;
    }

    public void CompleteLevel()
    {
        levelCompleteScreen.SetActive(true);
    }

    public void PlayScene(string sceneToPlay)
    {
        SceneManager.LoadScene(sceneToPlay);
    }
}
