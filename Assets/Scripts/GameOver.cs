﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    Button replayButton;
    GameManager gameManager;

    string sceneToReplay;
    // Start is called before the first frame update
    void Start()
    {
        sceneToReplay = SceneManager.GetActiveScene().name;
        gameManager = GameObject.FindObjectOfType<GameManager>();
        replayButton = GameObject.Find("ReplayButton").GetComponent<Button>();
        replayButton.onClick.AddListener(delegate { gameManager.PlayScene(sceneToReplay); });
    }
}
