﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour
{
    Button nextLevelButton;
    GameManager gameManager;

    public string nextScene;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        nextLevelButton = GameObject.Find("NextLevelButton").GetComponent<Button>();
        nextLevelButton.onClick.AddListener(delegate { gameManager.PlayScene(nextScene); });
    }
}