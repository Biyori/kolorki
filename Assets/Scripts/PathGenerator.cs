﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using PathCreation.Examples;

public class PathGenerator : MonoBehaviour
{
    PathCreator pathCreator;
    RoadMeshCreator roadMeshCreator;
    PathPlacer pathPlacer;
    GameObject player;
    Vector3 playerPos;

    float offset;
    bool isGenCooldown;
    float test;

    public float genCooldownValue;
    public float offsetBase;
    public float genDistance;
    //public float minY, maxY;
    //public float minZ, maxZ;

    // Start is called before the first frame update
    void Start()
    {
        pathCreator = GetComponent<PathCreator>();
        roadMeshCreator = GetComponent<RoadMeshCreator>();
        pathPlacer = GetComponent<PathPlacer>();
        player = GameObject.Find("Player");
        offset = offsetBase;
        isGenCooldown = false;
        test = 0;

    }

    // Update is called once per frame
    void Update()
    {
        GeneratePath();
        
    }

    void UpdatePath()
    {
        pathCreator.InitializeEditorData(false); // Inicjalizacja, ale nie pytaj mnie do czego to
        pathCreator.TriggerPathUpdate(); // Jakies updaty tez nie wiem jak to dziala ale czy to wazne
        roadMeshCreator.TriggerUpdate(); // ^^
        pathPlacer.Generate(); // Generacja obiektów na trasie (forcefieldy itp.)

    }

    void GeneratePath()
    {
        if (Vector3.Distance(player.transform.position, pathCreator.bezierPath.GetPointsInSegment(2)[0]) < genDistance && !isGenCooldown)
        {
            
            pathCreator.bezierPath.AddSegmentToEnd(new Vector3(0 - offset, 0, -18.49281f));//Random.Range(minY, maxY), Random.Range(minZ, maxZ)));
            offset += offsetBase;
            pathCreator.bezierPath.DeleteSegment(0);
            UpdatePath();

            isGenCooldown = true;
            Invoke("GenerationCooldown", genCooldownValue);
        }
    }

    void GenerationCooldown()
    {
        isGenCooldown = false;
    }
}
