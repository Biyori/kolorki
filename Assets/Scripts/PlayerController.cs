﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Renderer rend;
    GameManager gameManager;
    ScoreManager scoreManager;

    public Material dissolve;
    public Material[] materials;

    bool isDissolving;
    int currentMaterial;
    int materialsLength;
    float dissolveAmount;

    public float forceFieldScoreValue;
    public float changeColorScoreValue;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        gameManager = FindObjectOfType<GameManager>();
        scoreManager = FindObjectOfType<ScoreManager>();
        currentMaterial = 0;
        materialsLength = materials.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDissolving)
            rend.material = materials[currentMaterial];
        else
        {
            dissolveAmount = Mathf.Clamp01(dissolveAmount + Time.deltaTime);
            rend.material.SetFloat("_DissolveAmount", dissolveAmount);
        }

        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(TouchPhase.Began == touch.phase)
            {
                currentMaterial = (currentMaterial + 1) % materialsLength;
                scoreManager.AddToScore(changeColorScoreValue);
            }
        }

        if (Input.GetKeyDown("up"))
        {
            currentMaterial = (currentMaterial + 1) % materialsLength;
            scoreManager.AddToScore(changeColorScoreValue);
        }

        if (dissolveAmount == 1)
        {
            Destroy(this.gameObject); //Tutaj obiekt mozna zniszczyć tylko wtedy kamera staje w miejscu bo nie ma za czym podążać, więc zależy co chcemy osiągnąć. (Coś zniszczyć trzeba bo wywala bląd o tym, że shader od dissolva nie ma _EmissionColor jak przechodzi przez force fielda)
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("ForceField"))
        {
            Material collisionMat = collision.gameObject.GetComponent<Renderer>().material;
            if (rend.material.GetColor("_EmissionColor") == collisionMat.GetColor("_Emission"))
            {
                ForceField forceField = collision.GetComponent<ForceField>();
                forceField.Dissolve();
                scoreManager.AddToScore(forceFieldScoreValue);
                Debug.Log("Kolory pasują");
                //rzeczy ktore dzieja sie po przejsciu przez forcefield
            }
            else
            {
                dissolve.SetColor("_EdgeColor", rend.material.GetColor("_EmissionColor"));
                dissolve.SetColor("_Color", rend.material.GetColor("_Color"));
                isDissolving = true;
                rend.material = dissolve;
                gameManager.Invoke("GameOver", 0.3f);
                Debug.Log("Kolory nie pasują");
                //rzeczy ktore dzieja sie kiedy kolory nie pasuja
            }
        }
        else if (collision.gameObject.CompareTag("End"))
            gameManager.CompleteLevel();
    }
}
