﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    GameManager gameManager;

    [SerializeField]
    float score;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        score = 0;
    }

    public void AddToScore(float value)
    {
        if(!gameManager.IsGameOver())
            score += value;
    }

    public float GetScore()
    {
        return score;
    }
}
